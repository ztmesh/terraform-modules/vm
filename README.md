# vm

Module to create a single VM from a base template in Proxmox.


<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_proxmox"></a> [proxmox](#requirement\_proxmox) | 0.43.3 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_proxmox"></a> [proxmox](#provider\_proxmox) | 0.43.3 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |
| <a name="provider_tls"></a> [tls](#provider\_tls) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [proxmox_virtual_environment_vm.vm](https://registry.terraform.io/providers/bpg/proxmox/0.43.3/docs/resources/virtual_environment_vm) | resource |
| [random_password.vm_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [tls_private_key.vm_key](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cpu"></a> [cpu](#input\_cpu) | Number of CPU cores to assign | `number` | n/a | yes |
| <a name="input_disk_size"></a> [disk\_size](#input\_disk\_size) | Disk size to deploy in GB | `number` | `15` | no |
| <a name="input_ipv4_address"></a> [ipv4\_address](#input\_ipv4\_address) | IPv4 address to assign to this VM, with CIDR mask (eg. 10.0.0.1/24) | `string` | n/a | yes |
| <a name="input_ipv4_gateway"></a> [ipv4\_gateway](#input\_ipv4\_gateway) | IPv4 gateway for VM to use (eg. 10.0.0.254) | `string` | n/a | yes |
| <a name="input_memory"></a> [memory](#input\_memory) | Memory to assign in MB | `number` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Name of the VM | `string` | n/a | yes |
| <a name="input_node"></a> [node](#input\_node) | Name of the host which this VM is going to be placed on (eg. h02) | `string` | n/a | yes |
| <a name="input_os"></a> [os](#input\_os) | Friendly name of os template to deploy from (eg. debian12) | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ipv4_address"></a> [ipv4\_address](#output\_ipv4\_address) | IP of the VM |
| <a name="output_root_password"></a> [root\_password](#output\_root\_password) | Password for 'root' user |
| <a name="output_ssh_private_key"></a> [ssh\_private\_key](#output\_ssh\_private\_key) | Generated private key for 'root' user |
| <a name="output_ssh_public_key"></a> [ssh\_public\_key](#output\_ssh\_public\_key) | Generated public key, that has been placed in cloud-init keys |
| <a name="output_vm_password"></a> [vm\_password](#output\_vm\_password) | n/a |
| <a name="output_vm_private_key"></a> [vm\_private\_key](#output\_vm\_private\_key) | n/a |
| <a name="output_vm_public_key"></a> [vm\_public\_key](#output\_vm\_public\_key) | n/a |
<!-- END_TF_DOCS -->