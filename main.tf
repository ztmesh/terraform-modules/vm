locals {
  templates = {
    "debian12" : "9000"
  }
}

terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
      version = "0.43.3"
    }
  }
}

resource "proxmox_virtual_environment_vm" "vm" {
  name        = var.name
  description = "Managed by Terraform"

  node_name = var.node

  stop_on_destroy = true

  cpu {
    cores = var.cpu
  }

  memory {
    dedicated = var.memory
  }

  agent {
    enabled = false
  }

  startup {
    order      = "3"
    up_delay   = "60"
    down_delay = "60"
  }

  clone {
    datastore_id = "${var.node}-vm"
    vm_id        = local.templates[var.os]
  }

  disk {
    datastore_id = "${var.node}-vm"
    # file_id      = proxmox_virtual_environment_file.latest_ubuntu_22_jammy_qcow2_img.id
    interface   = "scsi0"
    file_format = "raw"
    size        = var.disk_size
  }

  initialization {
    ip_config {
      ipv4 {
        address = var.ipv4_address
        gateway = var.ipv4_gateway
      }
    }

    user_account {
      keys     = [trimspace(tls_private_key.vm_key.public_key_openssh)]
      password = random_password.vm_password.result
      username = "root"
    }

    datastore_id = "${var.node}-vm"

    # user_data_file_id = proxmox_virtual_environment_file.cloud_config.id
  }

  network_device {
    bridge = "vmbr0"
  }

  operating_system {
    type = "l26"
  }

  tpm_state {
    version      = "v2.0"
    datastore_id = "${var.node}-vm"
  }

  serial_device {}
}

resource "random_password" "vm_password" {
  length           = 16
  override_special = "_%@"
  special          = true
}

resource "tls_private_key" "vm_key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}