output "ipv4_address" {
    value = split("/", var.ipv4_address)[0]
    description = "IP of the VM"
}
output "ssh_public_key" {
    value = tls_private_key.vm_key.public_key_openssh
    description = "Generated public key, that has been placed in cloud-init keys"
}
output "ssh_private_key" {
    value = tls_private_key.vm_key.private_key_pem
    description = "Generated private key for 'root' user"
}
output "root_password" {
    value = random_password.vm_password.result
    description = "Password for 'root' user"
}
output "node" {
    value = var.node
    description = "Node the VM is hosted on"
}
output "name" {
    value = var.name
    description = "Hostname of the VM"
}
