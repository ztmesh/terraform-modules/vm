variable "name" {
  type = string
  description = "Name of the VM"
}

variable "node" {
  type = string
  description = "Name of the host which this VM is going to be placed on (eg. h02)"
}

variable "os" {
  type = string
  description = "Friendly name of os template to deploy from (eg. debian12)"
}

variable "cpu" {
  type = number
  description = "Number of CPU cores to assign"
}

variable "memory" {
  type = number
  description = "Memory to assign in MB"
}

variable "disk_size" {
  type = number
  description = "Disk size to deploy in GB"
  default = 15
}

variable "ipv4_address" {
  type = string
  description = "IPv4 address to assign to this VM, with CIDR mask (eg. 10.0.0.1/24)"
}

variable "ipv4_gateway" {
  type = string
  description = "IPv4 gateway for VM to use (eg. 10.0.0.254)"
}